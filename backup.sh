dirs=( boot etc home opt sbin srv usr )
uuid='d2f50d76-ece8-431c-9f01-bceb6a4ca5d3'
for dir in "${dirs[@]}"
do
   :
   echo $dir
   rsync --exclude '.cache' -vulr /$dir /run/media/demensdeum/$uuid/backup/
done
